import java.util.Scanner;

class Fahrkartenautomat3
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double rueckgabebetrag;

       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       // Geldeinwurf
       // -----------
       
       rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       rueckgeldAusgeben(rueckgabebetrag);

       
       tastatur.close(); //hatte gefehlt
    }
    public static double fahrkartenbestellungErfassen(Scanner the_scan)
    {
    	double ticketPreis;
    	double ticketanzahl;
    	double zuZahlenderBetrag;
    	
    	System.out.print("Preis eines Tickets (Euro): ");
        ticketPreis = the_scan.nextDouble();
        
        System.out.print("Anzahl der Tickets: ");
        ticketanzahl = the_scan.nextInt();
        
        zuZahlenderBetrag = ticketPreis * ticketanzahl; //Errechnen des zu bezahlenden Betrags
        System.out.printf("Zu zahlender Betrag (EURO): %.2f\n\n", zuZahlenderBetrag);
        
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner the_scan)
    {
    	double eingezahlterGesamtbetrag = 0.0;
    	double nochZuZahlen;
    	double eingeworfeneMuenze;
    	double rueckgabebetrag;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   nochZuZahlen = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n",nochZuZahlen);
     	   //Änderung von println zu printf, um den zuzahlenden Betrag mit nur zwei Nachkommstallen anzeigen zu lassen
     	   //Ebenfalls "Euro" hinzugefügt, damit zu ersehen ist, um was es sich handelt
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneMuenze = the_scan.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rueckgabebetrag;
    }
    
    public static void fahrkartenAusgeben()
    {
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag)
    {
        if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von: %.2f EURO\n",rueckgabebetrag); 
     	   //Änderung von println zu printf, um Dezimalstellen auf zwei zu beschränken
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
         	  rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	  rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	  rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
         	  rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
         	  rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
         	  rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}

/*Aufgabe 5:
Als Datentyp f�r die Anzahl der Tickets habe ich mich f�r int entschieden, da der Integer nur aus ganzen Zahlen
besteht und es nun mal keine halben, drittel oder viertel Tickets gibt.*/

/* Aufgabe 6:
Bei der Berechnung ticketPreis * ticketanzahl wird zun�chst ticketanzahl in den Datentypen double gecastet und
anschlie�end mit dem double ticketPreis multipliziert, wobei das Ergebnis dieser Multiplikation im double
zuZahlenderBetrag gespeichert wird.*/