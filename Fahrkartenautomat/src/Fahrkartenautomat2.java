import java.util.Scanner;

class Fahrkartenautomat2
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMuenze;
       double rueckgabebetrag;
       double nochZuZahlen; //gibt in jedem Durchlauf der While-Schleife an, wie viel noch zu Zahlen ist
       double ticketPreis; //Preis eines Tickets
       int ticketanzahl; //Anzahl der Tickets
       
       System.out.print("Preis eines Tickets (Euro): ");
       ticketPreis = tastatur.nextDouble();
       System.out.print("Anzahl der Tickets: ");
       ticketanzahl = tastatur.nextInt();
       zuZahlenderBetrag = ticketPreis * ticketanzahl; //Errechnen des zu bezahlenden Betrags
       System.out.printf("Zu zahlender Betrag (EURO): %.2f\n\n", zuZahlenderBetrag);
       //zuZahlenderBetrag = tastatur.nextDouble();
       

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   nochZuZahlen = zuZahlenderBetrag - eingezahlterGesamtbetrag;
    	   System.out.printf("Noch zu zahlen: %.2f Euro\n",nochZuZahlen);
    	   //Änderung von println zu printf, um den zuzahlenden Betrag mit nur zwei Nachkommstallen anzeigen zu lassen
    	   //Ebenfalls "Euro" hinzugefügt, damit zu ersehen ist, um was es sich handelt
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneMuenze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMuenze;
       }

       // Fahrscheinausgabe
       // -----------------
       if(ticketanzahl == 1)
       {
    	   System.out.println("\nFahrschein wird ausgegeben");
       }
       else
       {
    	   System.out.println("\nFahrscheine werden ausgegeben");
       }
       //Abfrage ob ein Fahrschein oder mehr
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rueckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von: %.2f EURO\n",rueckgabebetrag); 
    	   //Änderung von println zu printf, um Dezimalstellen auf zwei zu beschränken
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
        	  rueckgabebetrag -= 2.0;
           }
           while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
        	  rueckgabebetrag -= 1.0;
           }
           while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
        	  rueckgabebetrag -= 0.5;
           }
           while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
        	  rueckgabebetrag -= 0.2;
           }
           while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
        	  rueckgabebetrag -= 0.1;
           }
           while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
        	  rueckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
       tastatur.close(); //hatte gefehlt
    }
}