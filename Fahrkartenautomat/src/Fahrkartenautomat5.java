import java.util.Scanner;

class Fahrkartenautomat5
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	while (true)
       {
       
      
       double zuZahlenderBetrag; 
       double rueckgabebetrag;

       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       // Geldeinwurf
       // -----------
       
       rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       rueckgeldAusgeben(rueckgabebetrag);

       
       
       }
    }
    public static double fahrkartenbestellungErfassen(Scanner the_scan)
    {
    	int ticketWahl = 0;
    	double ticketanzahl = 0;
    	double zuZahlenderBetrag = 0;
    	double ticketPreis = 0;    	
    	boolean kartenbestellung_laeuft = true;
    	String[] fahrscheine = new String[] {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
    											"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
    											"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte ABC"};
    	double[] ticketpreise = new double[] {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
    	
    	/*Das Arbeiten mit Arrays vereinfacht in diesem Fall die Arbeit deutlich. Die Schreibarbeit verringert sich immens. Nun ist es auch deutlich leichter neue
    	 * Fahrkarten samt Preisen hinzuzuf�gen, da nicht extra eine neue switch-Ancweisung geschrieben werden muss. Probleme kann bei der Arbeit mit dem Array in diesem
    	 * Fall die L�nge des Arrays bereiten, da diese immer um eins h�her ist als die letzte Stelle des Arrays, da der Index des Arrays an Stelle 0 und nicht an Stelle 1
    	 * beginnt. Ich vermute, dass f�r nicht-programmier-affine Menschen jedoch die Arbeit ohne Arrays �bersichtlicher sein k�nnte*/
    	
    	while(kartenbestellung_laeuft == true)
    	{
    		boolean ticketanzahl_ist_ok = false;
    		boolean test_ticketwahl_ok = false;
    		
	    	System.out.println("\nBitte w�hlen Sie Ihre Wunschfahrkarte aus: ");
	    	System.out.printf("Nr.\t%-35s%15s\n", "Fahrschein", "Preis");
	    	
	    	
	    	for(int index = 0; index < fahrscheine.length; index++)
	    	{
	    		System.out.printf("%d\t%-35s%15.2f\n", index + 1, fahrscheine[index], ticketpreise[index]);
	    	}
	
	    	while(!test_ticketwahl_ok)
	    	{
	        	System.out.print("Ihre Wahl: ");
	            ticketWahl = the_scan.nextInt();
	            
	            if(ticketWahl > fahrscheine.length||ticketWahl < 1)
	            	System.out.println(">>Falsche Eingabe<<");
	            else
	            {
	            	ticketPreis = ticketpreise[ticketWahl - 1];
	            	test_ticketwahl_ok = true;
	            }
	    	}
	            
			    
	        
	        System.out.print("Anzahl der Tickets: ");
	        
	        while(ticketanzahl_ist_ok == false)
	        {
	        	ticketanzahl = the_scan.nextInt();
	        	if(ticketanzahl < 1||ticketanzahl > 10)
	        		System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
	        	else
	        		ticketanzahl_ist_ok = true;
	        }
	        
	        zuZahlenderBetrag += ticketPreis * ticketanzahl; //Errechnen des zu bezahlenden Betrags
	        
	        System.out.println("M�chten Sie weitere Fahrkarten kaufen? ja(1), nein(0)");
	        int weiterkaufen = the_scan.nextInt();
	        if(weiterkaufen == 0)
	        {
	        	kartenbestellung_laeuft = false;
	        }
    	}
        System.out.printf("Zu zahlender Betrag (EURO): %.2f\n\n", zuZahlenderBetrag);
        
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner the_scan)
    {
    	double eingezahlterGesamtbetrag = 0.0;
    	double nochZuZahlen;
    	double eingeworfeneMuenze;
    	double rueckgabebetrag;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   nochZuZahlen = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n",nochZuZahlen);
     	   //Änderung von println zu printf, um den zuzahlenden Betrag mit nur zwei Nachkommstallen anzeigen zu lassen
     	   //Ebenfalls "Euro" hinzugefügt, damit zu ersehen ist, um was es sich handelt
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneMuenze = the_scan.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rueckgabebetrag;
    }
    
    public static void fahrkartenAusgeben()
    {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag)
    {
        if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von: %.2f EURO wird in folgenden M�nzen ausgezahlt:\n",rueckgabebetrag); 
     	   //Änderung von println zu printf, um Dezimalstellen auf zwei zu beschränken

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
         	  rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	  rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	  rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
         	  rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
         	  rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
         	  rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.\n");
    }
}

/*Aufgabe 5:
Als Datentyp f�r die Anzahl der Tickets habe ich mich f�r int entschieden, da der Integer nur aus ganzen Zahlen
besteht und es nun mal keine halben, drittel oder viertel Tickets gibt.*/

/* Aufgabe 6:
Bei der Berechnung ticketPreis * ticketanzahl wird zun�chst ticketanzahl in den Datentypen double gecastet und
anschlie�end mit dem double ticketPreis multipliziert, wobei das Ergebnis dieser Multiplikation im double
zuZahlenderBetrag gespeichert wird.*/

