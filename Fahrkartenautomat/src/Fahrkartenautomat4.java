import java.util.Scanner;

class Fahrkartenautomat4
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	while (true)
       {
       
      
       double zuZahlenderBetrag; 
       double rueckgabebetrag;

       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       // Geldeinwurf
       // -----------
       
       rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       rueckgeldAusgeben(rueckgabebetrag);

       
       
       }
    }
    public static double fahrkartenbestellungErfassen(Scanner the_scan)
    {
    	int ticketWahl = 0;
    	double ticketanzahl = 0;
    	double zuZahlenderBetrag = 0;
    	double ticketPreis = 0;    	
    	boolean kartenbestellung_laeuft = true;
    	
    	while(kartenbestellung_laeuft == true)
    	{
    		boolean ticketanzahl_ist_ok = false;
    		boolean test_ticketwahl_ok = false;
    		
	    	System.out.println("\nBitte w�hlen Sie Ihre Wunschfahrkarte f�r Berlin AB aus: ");
	    	System.out.println(" Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
	    	System.out.println(" Tageskarte Regeltarif AB [8,60 EUR] (2)");
	    	System.out.println(" Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
	    	System.out.println(" Bezahlen (9)\n");
	    	
	
	        
	        while (test_ticketwahl_ok == false)
	        {
	        	System.out.print("Ihre Wahl: ");
	            ticketWahl = the_scan.nextInt();
		        switch (ticketWahl)
			        {
			        case 1:
			        	ticketPreis = 2.9;
			        	test_ticketwahl_ok = true;
			        	break;
			        case 2:
			        	ticketPreis = 8.6;
			        	test_ticketwahl_ok = true;
			        	break;
			        case 3:
			        	ticketPreis = 23.5;
			        	test_ticketwahl_ok = true;
			        	break;
			        case 9:
			        	kartenbestellung_laeuft = false;
			        	test_ticketwahl_ok = true;
			        	ticketanzahl_ist_ok = true;
			        	break;
			        default:
			        	System.out.println(">>Falsche Eingabe<<");
			        	break;
			        }
	        }
	        
	        System.out.print("Anzahl der Tickets: ");
	        
	        while(ticketanzahl_ist_ok == false)
	        {
	        	ticketanzahl = the_scan.nextInt();
	        	if(ticketanzahl < 1||ticketanzahl > 10)
	        		System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
	        	else
	        		ticketanzahl_ist_ok = true;
	        }
	        
	        if(ticketWahl == 1||ticketWahl == 2||ticketWahl == 3)
	        {
	        	zuZahlenderBetrag += ticketPreis * ticketanzahl; //Errechnen des zu bezahlenden Betrags
	        }
    	}
        System.out.printf("Zu zahlender Betrag (EURO): %.2f\n\n", zuZahlenderBetrag);
        
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner the_scan)
    {
    	double eingezahlterGesamtbetrag = 0.0;
    	double nochZuZahlen;
    	double eingeworfeneMuenze;
    	double rueckgabebetrag;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   nochZuZahlen = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n",nochZuZahlen);
     	   //Änderung von println zu printf, um den zuzahlenden Betrag mit nur zwei Nachkommstallen anzeigen zu lassen
     	   //Ebenfalls "Euro" hinzugefügt, damit zu ersehen ist, um was es sich handelt
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneMuenze = the_scan.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rueckgabebetrag;
    }
    
    public static void fahrkartenAusgeben()
    {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgabebetrag)
    {
        if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von: %.2f EURO wird in folgenden M�nzen ausgezahlt:\n",rueckgabebetrag); 
     	   //Änderung von println zu printf, um Dezimalstellen auf zwei zu beschränken

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
         	  rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	  rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	  rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
         	  rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
         	  rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
         	  rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.\n");
    }
}

/*Aufgabe 5:
Als Datentyp f�r die Anzahl der Tickets habe ich mich f�r int entschieden, da der Integer nur aus ganzen Zahlen
besteht und es nun mal keine halben, drittel oder viertel Tickets gibt.*/

/* Aufgabe 6:
Bei der Berechnung ticketPreis * ticketanzahl wird zun�chst ticketanzahl in den Datentypen double gecastet und
anschlie�end mit dem double ticketPreis multipliziert, wobei das Ergebnis dieser Multiplikation im double
zuZahlenderBetrag gespeichert wird.*/