import java.util.Scanner;

class Aufgabe3
{
	
	public static void main(String args[])
	{
		Scanner eingabe = new Scanner(System.in);
		
		double a;
		double b;
		double c;
		double h;
		double r;
		double pi = 3.14159265359;
		double ergebnis;
		//double ergebnis2;
		
		System.out.println("Berechnung des Volumens eines W�rfels\n");
		System.out.println("-------------------------------------------------\n");
		System.out.println("Bitte geben Sie die Kantenl�nge a des W�rfels an:");
		
		a = eingabe.nextDouble();
		ergebnis = volumenW(a);
		System.out.printf("Das Volumen des W�rfels betr�gt: %f\n\n", ergebnis);
		
		System.out.println("\nBerechnung des Volumens eines Quaders\n");
		System.out.println("-----------------------------------------------------------------\n");
		System.out.println("Bitte geben Sie die weiteren Kantenl�ngen des Quaders an (b, c):");

		b = eingabe.nextDouble();
		c = eingabe.nextDouble();
		ergebnis = volumenQ(a,b,c);
		System.out.printf("Das Volumen des Quaders betr�gt: %f", ergebnis);
		
		System.out.println("\nBerechnung des Volumens einer Pyramide\n");
		System.out.println("-----------------------------------------------------------------\n");
		System.out.println("Bitte geben Sie die H�he h der Pyramide an:");
		
		h = eingabe.nextDouble();
		ergebnis = volumenP(a,h);
		System.out.printf("Das Volumen der Pyramide betr�gt: %f", ergebnis);
		
		System.out.println("\nBerechnung des Volumens einer Kugel\n");
		System.out.println("-----------------------------------------------------------------\n");
		System.out.println("Bitte geben Sie den Radius der Kugel an:");
		
		r = eingabe.nextDouble();
		ergebnis = volumenK(r,pi);
		System.out.printf("Das Volumen der Kugel betr�gt: %f", ergebnis);
		
		eingabe.close();
	}
	
	public static double volumenW(double a)
	{
		double erg = a * a * a;
		return erg;
	}
	
	public static double volumenQ(double a, double b, double c)
	{
		double erg = a * b * c;
		return erg;
	}
	
	public static double volumenP(double a, double h)
	{
		double erg = a * a * h / 3;
		return erg;
	}
	
	public static double volumenK(double r, double pi)
	{
		double erg = (double)4/3 * r * r * r * pi;
		return erg;
	}
}