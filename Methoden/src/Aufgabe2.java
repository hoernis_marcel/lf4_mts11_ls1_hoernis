class Aufgabe2
{
	
	public static void main(String args[])
	{
		double wert1 = 2.36;
		double wert2 = 7.87;
		
		double erg = multiplikation(wert1,wert2);
		System.out.printf("%f * %f = %f", wert1,wert2,erg);
		
	}
	
	public static double multiplikation(double wert1,double wert2)
	{
		double ergebnis = wert1 * wert2;
		return ergebnis;
	}
}