import java.util.Scanner;

class Aufgabe1a
{
	public static void main(String[] args)
	{
		Scanner eingabe = new Scanner (System.in);
		
		double zahl1;
		double zahl2;
		
		System.out.print("Zahl�berpr�fung!\n");
		
		System.out.println("Bitte geben Sie zwei zu �berpr�fende Zahlen ein: ");
		zahl1 = eingabe.nextDouble();
		zahl2 = eingabe.nextDouble();
		
		System.out.print("Programm 1\n");
		
		if(zahl1 == zahl2)
		{
			System.out.println("Toll, du kannst zwei gleiche Zahlen eingeben!\n");
		}
		eingabe.close();
		
		System.out.print("Programm 2\n");
		
		if (zahl2 > zahl1)
		{
			System.out.println("Zahl 2 ist gr��er als Zahl 1!\n");
		}
		
		System.out.print("Programm 3\n");
		
		if(zahl1 >= zahl2)
		{
			System.out.println("Zahl 1 ist gr��er als Zahl 2!\n");
		}
		else
		{
			System.out.println("Zahl 2 ist gr��er als Zahl 1!\n");
		}
	}
	
}