import java.util.Scanner;

class Aufgabe1b
{
	public static void main(String[] args)
	{
		Scanner eingabe = new Scanner(System.in);
		double zahl1;
		double zahl2;
		double zahl3;
		
		System.out.println("Zahl�berpr�fung!");
		System.out.println("Bitte geben Sie drei Zahlen ein: ");
		
		zahl1 = eingabe.nextDouble();
		zahl2 = eingabe.nextDouble();
		zahl3 = eingabe.nextDouble();
		
		System.out.println("Programm 1");
		
		if(zahl1 > zahl2 && zahl1 > zahl3)
		{
			System.out.println("Zahl 1 ist gr��er als Zahl 2 & 3!");
		}
		
		System.out.println("Programm 2");
		
		if(zahl3 > zahl1 || zahl3 > zahl2)
		{
			System.out.println("Zahl 3 ist gr��er als Zahl 1 oder 2!");
		}
		
		System.out.println("Programm 3");
		
		if(zahl1 > zahl2 && zahl1 > zahl3)
		{
			System.out.println("Zahl 1 ist die gr��te Zahl.");
		}
		else if(zahl2 > zahl1 && zahl2 > zahl3)
		{
			System.out.println("Zahl 2 ist die gr��te Zahl.");
		}
		else if(zahl3 > zahl1 && zahl3 > zahl2)
		{
			System.out.println("Zahl 3 ist die gr��te Zahl.");
		}
		else
		{
			System.out.println("Die Zahlen sind gleich gro�.");
		}
		eingabe.close();
	}
}