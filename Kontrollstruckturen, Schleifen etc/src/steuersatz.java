import java.util.Scanner;

class steuersatz
{
	public static void main(String[] args)
	{
		Scanner eingabe = new Scanner(System.in);
		
		double nettowert;
		char steuersatz;
		double preis;
		
		System.out.println("Bitte geben Sie einen Nettowert in Euro ein: ");
		nettowert = eingabe.nextDouble();
		
		System.out.println("Bitte geben Sie ein ob es sich nei dem Preis um den erm��igten (j) oder den vollen (n) Steuersatz handelt: ");
		steuersatz = eingabe.next().charAt(0);
		
		if(steuersatz == 'j')
		{
			preis = nettowert + nettowert * 0.07;
		}
		else if(steuersatz == 'n')
		{
			preis = nettowert + nettowert  * 0.19;
		}
		else
		{
			preis = 0;
		}
		
		System.out.printf("Der Bruttopreis betr�gt: %.2f", preis);
		eingabe.close();
	}
}