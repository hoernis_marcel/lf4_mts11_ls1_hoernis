import java.util.Scanner;

class Quadrat
{
	public static void main(String args[])
	{
		Scanner the_scan = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Seitenlšnge des Quadrats ein: ");
		int seitenlaenge = the_scan.nextInt();
		for(int i = 0; i < seitenlaenge; i++)
		{
			for(int j = 0; j < seitenlaenge; j++)
			{
				if(j == 0|| j == seitenlaenge - 1||i == 0||i == seitenlaenge - 1)
					System.out.print("*");
				else
					System.out.print(" ");
				System.out.print(" ");
			}
			System.out.printf("\n");
		}
		the_scan.close();		
	}
}