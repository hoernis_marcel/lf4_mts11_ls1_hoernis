import java.util.Scanner;

class Matrix
{
	public static void main(String args[])
	{
		Scanner the_scan = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl zwischen 2 & 9 ein: ");
		int ueberpruefe_zahl = the_scan.nextInt();
		int zahllaufvariable = 0;
		int i = 0;
		int j = 0;
		do
		{
			do
			{
				boolean is_quersumme = quersumme(zahllaufvariable, ueberpruefe_zahl);
				boolean teilbarkeit = teilbar(zahllaufvariable, ueberpruefe_zahl);
				boolean is_enthalten = enthalten(zahllaufvariable, ueberpruefe_zahl);
				if(is_quersumme == true||teilbarkeit == true||is_enthalten == true)
					System.out.print("*");
				else
					System.out.print(zahllaufvariable);
				j++;
				zahllaufvariable++;
				System.out.print("\t");
			}while(j < 10);
			j = 0;
			i++;
			System.out.print("\n");
		}while(i < 10);
		the_scan.close();
	}
	
	public static boolean teilbar(int zahl, int ueberpruefe_zahl)
	{
		int erg = zahl % ueberpruefe_zahl;
		if (erg == 0)
			return true;
		else
			return false;
	}
	public static boolean quersumme(int zahl, int ueberpruefe_zahl)
	{
		int quersumme = (zahl % 10) + ((int)(zahl / 10) % 10);
		if(quersumme == ueberpruefe_zahl)
			return true;
		else
			return false;
	}
	public static boolean enthalten(int zahl, int ueberpruefe_zahl)
	{
		if(zahl % 10 == ueberpruefe_zahl||zahl / 10 == ueberpruefe_zahl)
			return true;
		else
			return false;
		
	}
}