import java.util.Scanner;

class Treppe
{
	public static void main(String args[])
	{
		Scanner the_scan = new Scanner(System.in);
		System.out.println("Bitte geben Sie die H�he der Treppe ein: ");
		int hoehe = the_scan.nextInt();
		System.out.println("Bitte geben Sie die L�nge der Treppenstufen ein: ");
		int stufenlaenge = the_scan.nextInt();
		int maxlaenge = stufenlaenge * hoehe;
		int sternpruefer = 0;
		for(int i = 0; i < hoehe; i++)
		{
			sternpruefer += stufenlaenge;
			for(int j = 0; j < maxlaenge; j++)
			{
				if(j >= (maxlaenge - sternpruefer))
					System.out.print("*");
				else
					System.out.print(" ");
			}
			System.out.print("\n");
		}
		the_scan.close();	
	}
}