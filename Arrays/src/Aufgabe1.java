import java.util.Scanner;

public class Aufgabe1 {
	public static void main(String[] args)
	{
		double[][] temperaturen;
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Bitte geben Sie die gr��e der Temperaturentabelle an: ");
		int groesse = scan.nextInt();
		
		temperaturen = tabellefuellen(groesse);
		
		System.out.println("Fahrenheit\tCelsius");
		for(int index = 0; index < groesse; index++)
		{
			System.out.printf("%.2f\t\t%.2f\n", temperaturen[index][0], temperaturen[index][1]);
		}
		scan.close();
	}
	
	public static double[][] tabellefuellen(int groesse)
	{
		double[][] temperaturen = new double[groesse][2];
		double temp_fahrenheit = 0.0;
		for(int index = 0; index < groesse; index++)
		{
			temperaturen[index][0] = temp_fahrenheit;
			temperaturen[index][1] = (5.0/9.0) * (temp_fahrenheit - 32);
			temp_fahrenheit += 10.0;
		}
		return temperaturen;
	}

}
