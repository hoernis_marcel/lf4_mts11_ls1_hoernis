import java.util.Scanner;

public class Steckbrief {

	public static void main(String[] args)
	{
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Hallo lieber User, bitte geben Sie Ihren Namen an:");
		
		String name = myScanner.next();
		
		System.out.print("\nBitte geben Sie auch Ihr Alter an:");
		
		int alter = myScanner.nextInt();
		
		System.out.printf("\n\nName: %s", name);
		
		System.out.printf("\nAlter: %d", alter);
		
		myScanner.close();
	}
}
