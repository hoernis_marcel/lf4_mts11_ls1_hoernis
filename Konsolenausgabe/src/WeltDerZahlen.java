
public class WeltDerZahlen {
	public static void main(String[] args) {
	    
	    /*  *********************************************************
	    
	         Zuerst werden die Variablen mit den Werten festgelegt!
	    
	    *********************************************************** */
	    // Im Internet gefunden ?
	    // Die Anzahl der Planeten in unserem Sonnesystem                    
	    int anzahlPlaneten =  8;
	    
	    // Anzahl der Sterne in unserer Milchstra�e
	    long anzahlSterne = 200000000000l;
	    
	    // Wie viele Einwohner hat Berlin?
	    int bewohnerBerlin = 3645000;
	    
	    // Wie alt bist du?  Wie viele Tage sind das?
	    
	    short alterTage = 7734; 
	    
	    // Wie viel wiegt das schwerste Tier der Welt?
	    // Schreiben Sie das Gewicht in Kilogramm auf!
	    int gewichtKilogramm = 150000;
	    
	    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
	    int flaecheGroessteLand = 17130000;
	    
	    // Wie gro� ist das kleinste Land der Erde?
	    
	    float flaecheKleinsteLand = 0.44f;
	    
	    
	    
	    
	    /*  *********************************************************
	    
	         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
	    
	    *********************************************************** */
	    
	    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
	    System.out.println("Anzahl der Sterne in der Milchstra�e: " + anzahlSterne);
	    System.out.println("Anzahl der Bewohner Berlins: " + bewohnerBerlin);
	    System.out.println("Mein Alter in Tagen: " + alterTage);
	    System.out.println("Gewicht eines Blauwals in kg: " + gewichtKilogramm);
	    System.out.println("Landfl�che Russlands in km�: " + flaecheGroessteLand);
	    System.out.println("Landfl�che Vatikan in km�: " + flaecheKleinsteLand);
	    

	    
	    
	    
	    
	    System.out.println(" *******  Ende des Programms  ******* ");
	    
	  }
}
